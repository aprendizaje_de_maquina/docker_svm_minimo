FROM frolvlad/alpine-miniconda3
ADD . /code
COPY . /code
WORKDIR /code
RUN conda install --yes --file requirements.txt
ENTRYPOINT [ "python","SVM.py" ]